const { events, Job} = require("brigadier");
events.on("exec", () => {
    var job = new Job("do-nothing", "alpine:3.8");
    job.tasks = [
        "echo Hello from brigade",
        "echo pernalongaesteveaqui"
    ];
    job.run();
});